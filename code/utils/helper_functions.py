from datetime import datetime, timedelta


def seconds_until_noon():
    now = datetime.utcnow()
    noon = datetime.utcnow().replace(hour=12, minute=0, second=0, microsecond=0)

    if now.hour >= 12:
        noon = noon + timedelta(days=1)

    delta = int(noon.timestamp() - now.timestamp())

    return delta


def make_spoiler(text):
    return "||" + text + "||"