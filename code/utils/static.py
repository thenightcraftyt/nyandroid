import os


class Staticstuff:
    texts = {"anarchism-in-a-nutshell": """Anarchism in a nutshell

        Anarchism is a social movement that seeks to abolish oppressive systems. Anarchists advocate a self-managed, classless, stateless society where everyone takes collective responsibility for the health and prosperity of their community.

        Anarchists are against coercive hierarchy. Anarchists believe that power corrupts, and that everyone should be treated equally.

        Anarchists seek to reduce or even end violence and oppression. The increasingly frequent misrepresentation of anarchism by the media to be about violence, nihilism, or disorder is completely false.

        All anarchists are anti-capitalism and anti-state. Capitalism is the economic system where investors and landlords are allowed to extract wealth from the economy without contributing goods or services back. Under capitalism, actual workers have little autonomy, or control over themselves. Instead, they are controlled by politicians and bankers.

        Anarchists advocate socialism instead of capitalism. Under socialism, workers have direct control of the means of production, or the land, factories, and offices. Through democratic organization, anarchists seek to remove the abusable systems of power that bosses and politicians leverage today to unjustly rule over society. Anarchists want to give everyone complete control over that which affects them.

        Most anarchists are communists, and advocate a "classless, moneyless, stateless society." Others are mutualists, and advocate "free market socialism". Anarchist society has no central authority, but instead consists of interconnected communities that use direct democracy (specifically, consensus) to organize themselves without rulers or bosses.
    """}
    command_prefix = "!"

    julia_user_id = 687740609703706630  # 209360364188598272 RIP
    aggie_user_id = 254310746450690048

    admin_role_id = 689041552131096593
    mod_role_id = 689047640142250047
    valid_role_id = 689047914265182237
    bot_role_id = 689048022117777578

    anarchy_role_id = 689048022902112299
    vent_support_role_id = 692457125397397574
    nsfw_role_id = 692457218083127297
    stem_role_id = 692457288379793439
    health_transition_talk_role_id = 692485260230787132

    she_pronoun_id = 689048023627858000
    they_pronoun_id = 689048024512593950
    he_pronoun_id = 689048025255247886
    it_pronoun_id = 692475814985596989

    purple_role_id = 689048003885138103
    blurple_role_id = 689048017957027916
    blue_role_id = 689048017319362583
    cyan_role_id = 689048018624053349
    green_role_id = 689048019517177870
    yellow_role_id = 689048020658028595
    orange_role_id = 689048020553039903
    red_role_id = 689048021522055301

    anarchy_info_channel_id = 689063111961083921
    anarchy_general_two_id = 705911831012704309
    protected_channel_id = 689063198506090520
    anarchy_log_id = 689063150980431909
    ruleconfirm_channel_id = 689077340239560708
    ruleconfirm_log_id = 689077430211969025
    selfies_channel_id = 689056306434408474
    selfies_chat_id = 689056339988971564
    support_channel_id = 689056648681357344
    venting_channel_id = 689056634311409672
    venting_2_electric_boogaloo = 697842470108659742
    general_channel_id = 689023662489468966
    bot_forbidden_log_id = 689104197777752105
    bot_spam_channel_id = 689106682969718810
    announcements_channel_id = 689134844441002049
    mod_talk_channel_id = 689052158674468905
    welcome_channel_id = 689055834453573632

    age_13_id = 699041189600559155
    age_14_id = 699041214858788864
    age_15_id = 699041215886262363
    age_16_id = 699041218142929036
    age_17_id = 699041219510009907
    age_18_id = 699041217350074440

    ace_role_id = 699041217031176313
    aro_role_id = 699041218423816273
    lesbian_role_id = 699041215340871703
    gay_role_id = 699061980492857456
    straight_role_id = 699061994795565056
    bi_role_id = 699062011543421009
    pan_role_id = 699062005767864330
    poly_role_id = 699061983030542376
    abrosexual_role_id = 699062014089363536
    gynesexual_role_id = 699085288365883412
    genderfluid_role_id = 702276818081808475
    wtf_sexuality_role_id = 701585719931568148  # first 70... id in these roles OwO

    female_role_id = 699062019059613796
    male_role_id = 699061984762789910
    enby_role_id = 699061995705729055
    cis_role_id = 699062021760483370
    agender_role_id = 699062024096972910
    demigirl_role_id = 699062002257231922
    demiboy_role_id = 699062016740163644
    fem_role_id = 699061955109060671
    masc_role_id = 699062008049434787
    wtf_gender_role_id = 701585724813869076  # maybe rename these to gender/sexuality-questioning later

    support_command_use_channels = [support_channel_id, venting_channel_id, venting_2_electric_boogaloo]
    support_command_channels = [general_channel_id]

    protected_anarchy_channels = [protected_channel_id, anarchy_info_channel_id, anarchy_log_id, anarchy_general_two_id]

    access_roles = {
        "anarchy": anarchy_role_id,
        "venting": vent_support_role_id,
        "nsfw": nsfw_role_id,
        "stem": stem_role_id,
        "health-trans-talk": health_transition_talk_role_id
    }

    color_roles = {
        "purple": purple_role_id,
        "blurple": blurple_role_id,
        "blue": blue_role_id,
        "cyan": cyan_role_id,
        "green": green_role_id,
        "yellow": yellow_role_id,
        "orange": orange_role_id,
        "red": red_role_id
    }

    pronoun_roles = {
        "she": she_pronoun_id,
        "he": he_pronoun_id,
        "they": they_pronoun_id,
        "it": it_pronoun_id
    }

    age_roles = {
        "13": age_13_id,
        "14": age_14_id,
        "15": age_15_id,
        "16": age_16_id,
        "17": age_17_id,
        "18+": age_18_id
    }

    sexuality_roles = {
        "ace": ace_role_id,
        "aro": aro_role_id,
        "lesbian": lesbian_role_id,
        "gay": gay_role_id,
        "straight": straight_role_id,  # wtf is wrong with people (ok yeah i'm probably a bit heterophobic ngl)
        "bi": bi_role_id,
        "pan": pan_role_id,
        "poly": poly_role_id,
        "abrosexual": abrosexual_role_id,
        "gynesexual": gynesexual_role_id,
        "wtf-is-sexuality": wtf_sexuality_role_id
    }

    gender_identity_roles = {
        "female": female_role_id,
        "male": male_role_id,
        "nonbinary": enby_role_id,
        "cis": cis_role_id,  # lmao imagine being cis *wipes away tears from crying about not being a cis girl*
        "agender": agender_role_id,
        "demigirl": demigirl_role_id,
        "demiboy": demiboy_role_id,
        "fem": fem_role_id,
        "masc": masc_role_id,
        "genderfluid": genderfluid_role_id,
        "wtf-is-gender": wtf_gender_role_id
    }

    roles = {
        "category access": access_roles,
        "name colors": color_roles,
        "pronouns": pronoun_roles,
        "gender identities": gender_identity_roles,
        "sexualities": sexuality_roles,
        "age": age_roles
    }

    roles_flattened = {}  # it was 4am please excuse this dirty hack | side note, grimes - 4aem FUCKING SLAPS YO WTF
    for category in roles:  # todo-maybe: convert this to dict comprehension but i can't comprehend that at 4am
        for key in roles[category]:
            roles_flattened[key] = roles[category][key]

    ban_on_join_ids = {
        377514339571335178: {
            "name": "kes",
            "reason": "fucking getting the server banned",
            "server_message": "automatically banning kes.\nshe got the server banned one time.",
            "dm_message": "you were banned from cute cat commune. you know why."
        },
        377521309934157824: {
            "name": "Hathor",
            "reason": "Kes' alt acount",
            "server_message": "automatically banning kes.\nshe got the server banned one time.",
            "dm_message": "you were banned from cute cat commune. you know why."
        },
        586421585359929345: {
            "name": "luigimama1",
            "reason": "going on LGBTQ+ discords and posting gore",
            "server_message": "automatically banning a user\nthey posted gore on other LGBTQ+ discords",
            "dm_message": "you were banned from cute cat commune because one of our users reported you for posting gore on other LGBTQ+ servers"
        }
    }

    mod_or_higher = [admin_role_id, mod_role_id]

    full_access_users = [julia_user_id, aggie_user_id]

    nyandroid_devs = [julia_user_id, aggie_user_id]  # users that can run maintenance commands

    i_love_you_variants = ["❤"]
    for i in ["love", "wuv", "luv"]:
        for j in ["you", "u"]:
            i_love_you_variants.append("i " + i + " " + j)

    invite_link = "https://discord.gg/adc6DJh"

    images = {
        "sayori no": "https://cdn.discordapp.com/attachments/693237869380370485/693511894283059210/sayori_no.png",
        "sayori yes": "https://cdn.discordapp.com/attachments/693237869380370485/693511884405473319/sayori_yes.png",
        "sayori hyperyes": "https://cdn.discordapp.com/attachments/693237869380370485/693511879091421264/sayori_hyper_yes.png",
        "mario no": "https://cdn.discordapp.com/attachments/693237869380370485/693237962749509733/marioNO.png",
        "cow perhaps": "https://cdn.discordapp.com/attachments/693237869380370485/693520509035610112/cow_perhaps.png",
        "clown 1": "https://cdn.discordapp.com/attachments/693237869380370485/704839421329145886/clown_1.png",
        "clown 2": "https://cdn.discordapp.com/attachments/693237869380370485/704839427926786139/clown_2.png",
        "clown 3": "https://cdn.discordapp.com/attachments/693237869380370485/704839425934360576/clown_3.png",
        "clown 4": "https://cdn.discordapp.com/attachments/693237869380370485/704839423988334662/clown_4.png",
        "kalm": "https://cdn.discordapp.com/attachments/693237869380370485/704839423036227636/kalm.png",
        "panik": "https://cdn.discordapp.com/attachments/693237869380370485/704839419646967809/panik.png",
        "panik 2": "https://cdn.discordapp.com/attachments/693237869380370485/704839417524650014/panik2.png",
        "horny jail": "https://cdn.discordapp.com/attachments/693237869380370485/706230909568942080/hornyjail.jpg"
    }

    user_voice_channels = {}

    birthdays = []  # birthdays are stored internally as a list of id, name, date(datetime object)

    version = "2.1"
