import asyncio
from . import helper_functions as hf


def async_run_at_noon(func):
    r"""for use with classes"""
    async def wrapper(self, *args, **kwargs):
        while True:
            await asyncio.sleep(hf.seconds_until_noon())
            await func(self, *args, **kwargs)
            await asyncio.sleep(5)  # seconds_until_noon() will return 0 if it's exactly 12:00:00
    return wrapper


def wait_until_ready(func):
    r"""needs a function wait_until_ready() in the class"""
    async def wrapper(self, *args, **kwargs):
        await self.wait_until_ready()
        await func(self, *args, **kwargs)
    return wrapper


def _async_run_every_second(func):
    """owo testing"""
    async def wrapper(self, *args, **kwargs):
        while True:
            await func(self, *args, **kwargs)
            await asyncio.sleep(1)
    return wrapper
