import asyncio
import random
import time
from datetime import date, datetime, timedelta
# import importlib as il
from urllib.parse import quote as urlencode
# import logging
import json
import os
from utils.static import Staticstuff  # haha error gon
import utils.decorators
# from UnitConverter import unit_converter as uc # TODO: get this working lmao, recode it if need be
import subprocess
import discord
from discord.utils import get
import aiohttp
import io

# logger = logging.getLogger('discord')  # copied from https://docs.python.org/3/howto/logging.html#logging-basic-tutorial
# logger.setLevel(logging.INFO)
# handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
# handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
# logger.addHandler(handler)

try:
    with open("token") as f:
        TOKEN = f.read().rstrip()
except FileNotFoundError:
    try:
        with open("code/token") as f:
            TOKEN = f.read().rstrip()
    except FileNotFoundError:
        print(
            "make sure you run this from nyandroid/ or nyandroid/code/, and that you have the token file present")
        exit(1)

PERM_INT = 1006894199
SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))  # does not include the filename


class NyandroidClient(discord.Client, Staticstuff):
    data_dir = os.path.join(SCRIPT_PATH, "data")
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    if not os.path.isfile(os.path.join(data_dir, "birthdays.json")):  # set default file contents
        with open(os.path.join(data_dir, "birthdays.json"), "w") as f:
            f.write("{}")

    def write_birthdays(self):
        self.sort_birthdays()
        with open(os.path.join(self.data_dir, "birthdays.json"), "w") as f:
            bds = {}
            for bd in self.birthdays:
                userid, name, d = bd
                bds[userid] = [name, d.strftime("%b %d")]

            f.write(json.dumps(bds))

    def sort_birthdays(self):
        self.birthdays = sorted(self.birthdays, key=lambda entry: entry[2])

    def read_birthdays(self):
        with open(os.path.join(self.data_dir, "birthdays.json"), "r") as f:
            bds = json.loads(f.read())
            self.birthdays = [[int(key), bds[key][0], datetime.strptime(bds[key][1], "%b %d")] for key in bds]
            self.sort_birthdays()  # it's always gonna be done so whatever

    def get_bd_index(self, userid):
        for i in range(len(self.birthdays)):
            if self.birthdays[i][0] == userid:
                return i
        raise IndexError("User ID not found in birthday list")

    def user_has_registered_bd(self, userid):
        try:
            index = self.get_bd_index(userid)
            return index
        except IndexError:
            return False

    async def add_bd(self, message, userid, name, bd):
        self.birthdays.append([int(userid), name, bd])
        self.sort_birthdays()
        self.write_birthdays()
        await message.channel.send(f"Success: Added your birthday ({bd.strftime('%B %d')}) with name {name}")

    @staticmethod
    def get_day_suffix(day):  # totally my code, not copied from anywhere.
        day = int(day)
        suffix = ["th", "st", "nd", "rd"]
        return suffix[day % 10] if day % 10 in [1, 2, 3] and day not in [11, 12, 13] else suffix[0]

    @utils.decorators.wait_until_ready
    @utils.decorators.async_run_at_noon
    async def announce_bd(self):
        today = date.today()
        general = self.get_channel(self.general_channel_id)
        for _, name, bd in self.birthdays:
            bd = bd.date().replace(year=today.year)
            name += "'" if name[-1] == "s" else "'s"
            if today == bd:
                await general.send(f"Nyaaaa~, it's **{name}** birthday! :tada:")
            if today + timedelta(days=7) == bd:
                await general.send(f"**{name}** birthday is in 1 week uwu~")

    async def send_forbidden_log(self, message):
        await message.guild.get_channel(self.bot_forbidden_log_id).send(
            "{0.author.mention} has attempted to use a forbidden command in {0.channel.mention}\ncommand: `{0.content}`".format(message))

    async def send_meme_image(self, channel, image, text="", color=0x800869):
        embed = discord.Embed(color=color)
        if text:
            embed.description = text
        embed.set_image(url=self.images[image])
        await channel.send(embed=embed)

    def _get_roles_as_string(self):
        out = ""
        max_len = max([len(key) for key in self.roles])
        for category in self.roles:
            cat = category + ":"
            out += "`" + cat.ljust(max_len + 2, " ") + ", ".join(self.roles[category]) + "`" + "\n"
        return out

    async def list_roles(self, channel):
        await channel.send(self._get_roles_as_string())
        await channel.send("if you want anything added, don't hesitate to contact staff!")

    async def on_ready(self):
        print("logged in as", self.user.name)
        print("id:", self.user.id)
        print("-----")

        self.read_birthdays()

        self.loop.create_task(self.announce_bd())  # is here because it'd run before birthdays is initialized

    async def on_voice_state_update(self, member, before, after):
        before_id = before.channel.id if before.channel else None
        after_id = after.channel.id if after.channel else None
        if before_id in self.user_voice_channels and after_id not in self.user_voice_channels:
            user_channel = self.user_voice_channels[before_id]
            if not user_channel.members:
                await user_channel.delete()
                self.user_voice_channels.pop(before_id)

    async def on_member_join(self, member):
        if member.id in self.ban_on_join_ids:
            announcements_channel = self.get_channel(self.announcements_channel_id)
            ban_data = self.ban_on_join_ids[member.id]

            try:
                await announcements_channel.send(ban_data["server_message"])
            except KeyError:
                pass

            try:
                dm_channel = await member.create_dm()
                await dm_channel.send(ban_data["dm_message"])
            except KeyError:
                pass
            except discord.errors.Forbidden:
                pass

            try:
                await member.ban(reason=ban_data["reason"])
            except:
                return await self.get_channel(self.mod_talk_channel_id).send("a user on our ban list just joined, and i cannot ban them for some reason, please take care of it @everyone")

    async def on_message(self, message):
        # print("message from", message.author.name + ":", message.content)
        if message.author.id == self.user.id:
            return

        if message.content.startswith(self.command_prefix):
            await self.on_command_prefix_message(message)

        elif message.channel.id == self.protected_channel_id:
            await message.delete()
            await message.channel.send("this is just for creating channels you dork")

        if message.channel.id == self.selfies_channel_id:  # todo: move to separate function
            if len(message.attachments) == 0:
                await message.delete()
            else:
                name = True  # this attaches the username on the first image
                for attach in message.attachments:
                    if attach.is_spoiler():
                        out = message.author.display_name + ":\n" if name else ""
                        name = not name if name else name  # name?
                        out += utils.helper_functions.make_spoiler(attach.url)
                        await message.guild.get_channel(self.selfies_chat_id).send(out)
                    else:
                        embed = discord.Embed(color=0xfeccff)

                        if name:
                            embed.set_author(name=message.author.display_name, icon_url=message.author.avatar_url)
                            embed.description = message.content
                            name = not name

                        embed.set_image(url=attach.url)

                        await message.guild.get_channel(self.selfies_chat_id).send(embed=embed)

        if message.channel.id == self.ruleconfirm_channel_id:  # todo: move to separate function
            if message.content.lower() == "i have read the rules":
                await message.channel.send("please type `confirm` to confirm that you have actually read them")
                try:
                    confirm_message = await self.wait_for('message', check=(
                        lambda m: m.author.id == message.author.id and m.content.lower() == "confirm"), timeout=30.0)
                except asyncio.TimeoutError:
                    return await message.channel.send("you were too slow (30 seconds)")

                if message.guild.get_role(self.valid_role_id) not in message.author.roles:
                    await message.author.add_roles(get(message.guild.roles, name="Valid"))
                    await message.channel.send("welcome to the server :3")
                    await message.guild.get_channel(self.ruleconfirm_log_id).send(
                        "{0.mention} has confirmed that they read the rules at {1} UTC\nthis message serves as confirmation of that\nlink to confirmation message: https://discordapp.com/channels/{2.guild.id}/{2.channel.id}/{2.id}".format(
                            message.author, time.asctime(confirm_message.created_at.timetuple()), message))

        if message.content.lower() in ["good nyandroid", "good bot"]:
            await message.channel.send("uwu")

        if message.content.lower() == "i am bot":
            await message.channel.send("no i am bot >:3")

        for ily in self.i_love_you_variants:
            if ily in message.content.lower() and random.randint(1, 10) % 10 == 0:  # 10% chance of the bot sending "❤" in response to an ily
                return await message.channel.send("❤")

    async def on_command_prefix_message(self, message):
        message_parts = message.content.split(" ")
        command = message_parts[0].split(self.command_prefix)[1]  # command without command prefix
        message_parts = message_parts[1:] if len(message_parts) > 1 else []

        if command == "role" or command == "roles":
            if "bot-spam" in message.channel.name or "anarchy" in str(self.get_channel(message.channel.category_id)):
                subcommand = message_parts[0] if message_parts else ""
                rolenames = message_parts[1:] if len(message_parts) > 1 else []
                if subcommand == "list":
                    return await self.list_roles(message.channel)
                elif subcommand == "add":
                    for rolename in rolenames:
                        if rolename in self.roles_flattened:
                            await message.author.add_roles(message.guild.get_role(self.roles_flattened[rolename]))
                            await message.channel.send(f"role {rolename} added")
                elif subcommand == "remove":
                    for rolename in rolenames:
                        if rolename in self.roles_flattened:
                            await message.author.remove_roles(message.guild.get_role(self.roles_flattened[rolename]))
                            await message.channel.send(f"role {rolename} removed")
                else:
                    await message.channel.send("`!role add [role-names]` - give yourself a role\n`!role remove [role-names]` - take a role away\n`!role list` - list all available roles")
                    await self.list_roles(message.channel)

        if command == "format":
            color = message.author.color
            author = message.author
            desc = " ".join(message_parts)

            embed = discord.Embed(color=color, description=desc)
            embed.set_footer(text=str(author), icon_url=author.avatar_url)

            await message.channel.send(embed=embed)
            await message.delete()

        if command == "selfiepls":
            selfies_channel = self.get_channel(self.selfies_channel_id)
            if message.channel.id == self.selfies_chat_id:
                selfie_message = await selfies_channel.history(limit=1).flatten()  # note to self: you can't access list elements before you have them, dork
                attachments = selfie_message[0].attachments
                if len(attachments):
                    await message.channel.send("here's your " + "image" if len(attachments) == 1 else "images")
                    for attach in selfie_message[0].attachments:
                        url = utils.helper_functions.make_spoiler(attach.url) if attach.is_spoiler() else attach.url
                        await message.channel.send(url)

        if command == "vcpls":
            category_id = message.channel.category_id  # can be none
            category = self.get_channel(category_id) if category_id else None
            channel_name = " ".join(message_parts) if message_parts else "default voice channel name"
            new_channel = await message.guild.create_voice_channel(channel_name, category=category)
            self.user_voice_channels[new_channel.id] = new_channel
            await message.channel.send(f"created voice channel \"{channel_name}\"" + f" in category {category.name}" if category_id else "")

        if command == "bdadm":
            if message.author.top_role.id in self.mod_or_higher:
                subcommand = message_parts[0] if message_parts else ""

                if subcommand == "add":
                    userid, month, day = message_parts[1:4]
                    d = " ".join((month, day))
                    name = " ".join(message_parts[4:])

                    bd = datetime.strptime(d, "%b %d")

                    await self.add_bd(message, userid, name, bd)

        if command in ["bd", "birthday"]:
            subcommand = message_parts[0] if message_parts else ""

            if subcommand == "add":
                month = message_parts[1] if len(message_parts) > 1 else ""
                day = message_parts[2].rjust(2, "0") if len(message_parts) > 2 else ""
                d = " ".join((month, day))
                name = " ".join(message_parts[3:]) if len(message_parts) > 3 else message.author.display_name
                userid = message.author.id

                try:
                    bd = datetime.strptime(d, "%b %d")
                except ValueError:
                    return await message.channel.send(f"Error: '{d}' does not match format ('Apr 23')")

                if type(self.user_has_registered_bd(userid)) == bool:  # the function returns false if there's no user, otherwise int(index)
                    await self.add_bd(message, userid, name, bd)
                else:
                    await message.channel.send("Error: you have already added a birthday!")

            if subcommand in ["delete", "remove"]:
                userid = int(message_parts[1]) if message.author.top_role.id in self.mod_or_higher and len(message_parts) > 1 else message.author.id  # fucking json module converting my ints to string and breaking my code making me debug at 4aem
                try:
                    index = self.get_bd_index(userid)
                    bd = self.birthdays.pop(index)
                    self.sort_birthdays()  # this is probably not necessary but sorting go brrrrrrr
                    self.write_birthdays()
                except IndexError:
                    return await message.channel.send("Nothing to remove")

                return await message.channel.send(f"Removed entry for user {bd[1]}")

            if subcommand == "list":
                if self.birthdays:
                    max_len = max([len(bd[1]) for bd in self.birthdays])
                    out = "```\n"
                    for bd in self.birthdays:
                        _, name, d = bd
                        out += name.rjust(max_len, " ") + " : " + d.strftime("%B %d") + "\n"
                    out += "```"
                    await message.channel.send(out)
                else:
                    await message.channel.send("No birthdays registered yet")

        if command == "help":  # lmao
            await message.channel.send("nyandroid help\n\n"
                                       "everywhere:\n"
                                       "`!guess [<number>]` - simple number guessing game, default 1 to 10\n"
                                       "`!echo text` - make the bot say something\n"
                                       "`!ping` - get bot latency\n"
                                       "`!invite` - get the invite link\n"
                                       "`!userinfo [chat]` - get some information about your account (in PM or in chat)\n"
                                       f"\n{self.get_channel(self.bot_spam_channel_id).mention}:\n"
                                       "`!role add [role-names]` - give yourself roles\n`!role remove [role-names]` - take roles away\n`!role list` - list all available roles\n"
                                       "\nanarchy:\n"
                                       "`!anarchy-nutshell` - learn a little bit about anarchy\n"
                                       "`!create [<channel name>]` - create a channel in the anarchy category\n"
                                       "`!delete` - delete the channel you're in\n"
                                       "\nsupport category:\n"
                                       "`!support` - alert the general channels")
            if message.author.top_role.id in self.mod_or_higher:
                await message.channel.send("-----\n\nMod specific:\n\n"
                                           "`!purge [all|<number>|<none>]` - deletes messages from the bottom up, no argument = 10 messages deleted\n"
                                           "`!purgefrom id` - deletes all messages below a certain message - use discord developer mode to get the ID\n"
                                           "`!purgebetween id1 id2` - deletes all messages between and including two certain messages - use discord developer mode to get IDs\n")

        if command == "clear":
            if message.author.top_role.id in self.mod_or_higher:
                reason = " ".join(message_parts) if message_parts else ""
                msg = "." + "\n." * 100 + "\ncleared UwU"
                msg += f"\nreason: \"{reason}\""if reason else ""
                await message.channel.send(msg)

        if command == "purge":
            if message.author.top_role.id in self.mod_or_higher:
                if "nomessage" in message_parts:
                    nomessage = True
                else:
                    nomessage = False

                try:
                    if "all" in message_parts:
                        messages = await message.channel.history().flatten()
                        return await message.channel.delete_messages(messages)
                    else:
                        lim = int(message_parts[0]) + 1
                except IndexError:
                    lim = 11

                deleted = await message.channel.purge(limit=lim)
                if lim and not nomessage:
                    await message.channel.send("deleted {} message(s)".format(len(deleted) - 1))
            else:
                await message.channel.send("nyo\nMods only :/")
                await self.send_forbidden_log(message)

        if command == "purgefrom":
            if message.author.top_role.id in self.mod_or_higher:
                try:
                    msg_id = int(message_parts[0])
                except IndexError:
                    return await message.channel.send("provide the message id as an argument (discord developer mode)")
                except ValueError:
                    return await message.channel.send("invalid message id")

                try:
                    nomessage = message_parts[1].lower() == "nomessage"
                except IndexError:
                    nomessage = False

                messages = await message.channel.history().flatten()

                delmsgs = []
                for msg in messages:
                    delmsgs.append(msg)
                    if msg.id == msg_id:
                        await message.channel.delete_messages(delmsgs)
                        break
                if not nomessage:
                    await message.channel.send("done :3\ndeleted {} messages uwu".format(len(delmsgs)))
            else:
                await message.channel.send("nyo\nMods only :/")
                await self.send_forbidden_log(message)

        if command == "purgebetween":
            if message.author.top_role.id in self.mod_or_higher:
                try:
                    id1, id2 = int(message_parts[0]), int(message_parts[1])
                except IndexError:
                    return await message.channel.send("provide two message ids as arguments (discord developer mode)")
                except ValueError:
                    return await message.channel.send("invalid message id")

                if "nomessage" in message_parts:
                    nomessage = True
                else:
                    nomessage = False

                messages = await message.channel.history().flatten()

                delete = False
                delmsgs = []
                for msg in messages:
                    if msg.id == id1 or msg.id == id2:
                        delete = not delete
                        if not delete:
                            delmsgs.append(msg)
                            break
                    if delete:
                        delmsgs.append(msg)
                if delete:
                    return await message.channel.send("didn't find one of the messages, not deleting anything")
                else:
                    await message.channel.delete_messages(delmsgs)
                    if not nomessage:
                        await message.channel.send("done :3\ndeleted {} messages uwu".format(len(delmsgs)))
                    pass
            else:
                await message.channel.send("nyo\nMods only :/")
                await self.send_forbidden_log(message)

        if command == "eval":
            if message.author.id in self.full_access_users:
                eval_text = " ".join(message_parts)
                embed = discord.Embed(color=0x800869)
                embed.add_field(name="evaluated code:", value="```py\n" + eval_text + "\n```", inline=False)

                try:
                    result = eval(eval_text)
                    if result is None:
                        result = "<no return value>"
                    error = False
                except Exception as e:
                    result = repr(e)
                    error = True

                embed.add_field(name="result:", value="```py\n" + str(result) + "\n```", inline=False)
                await message.channel.send(embed=embed)
            else:
                await message.channel.send("nyo\nthis incident has been reported")
                await self.send_forbidden_log(message)

        if command == "eval-quiet":
            if message.author.id in self.full_access_users:
                await message.channel.send(eval(" ".join(message_parts)))
                await message.delete()
            else:
                await message.channel.send("nyo\nthis incident has been reported")
                await self.send_forbidden_log(message)

        if command == "nyandroid-update":
            if message.author.id in self.full_access_users or message.author.id in self.nyandroid_devs:
                general = self.get_channel(self.general_channel_id)
                await general.send("beep boop i am updating myself, might be offline for a minute or so bleep bloop")
                await message.channel.send("restart initiated")
                subprocess.Popen(["/bin/bash", os.path.join(SCRIPT_PATH, "selfupdate.sh")])

        if command == "ping":
            await message.channel.send(f"Pong :3 ({int(self.latency * 1000)}ms)")

        if command == "no":
            await self.send_meme_image(message.channel, "mario no")

        if command == "perhaps":
            await self.send_meme_image(message.channel, "cow perhaps")

        if command == "clap":
            if not message_parts:
                messages = await message.channel.history(limit=2).flatten()
                message_parts = messages[1].content.split(" ")

            text = " :clap: ".join(message_parts)

            await message.channel.send(text)

        if command == "louderforthepeopleintheback":
            if not message_parts:  # get last message in channel
                messages = await message.channel.history(limit=2).flatten()
                message_parts = messages[1].content.split(" ")

            text = "**" + " ".join(message_parts).upper() + "**"
            await message.channel.send(text)

        if command == "hornyjail":
            user = message.mentions[0] if message.mentions else " ".join(message_parts) if message_parts else None
            targetname = user.display_name if message.mentions else user
            desc = f"{targetname}, {message.author.display_name} has sent you to horny jail" if user else None

            embed = discord.Embed(color=0x800869, description=desc)
            embed.set_footer(text=str(message.author), icon_url=message.author.avatar_url)
            embed.set_image(url=self.images["horny jail"])

            await message.channel.send(embed=embed)

        if command == "support":
            if message.channel.id in self.support_command_use_channels or "anarchy" in str(
                    self.get_channel(message.channel.category_id)):
                for channel_id in self.support_command_channels:
                    await message.guild.get_channel(channel_id).send(
                        "❤️ {0.author.display_name} is looking for support in {0.channel.mention} :3".format(message))
            else:
                await message.channel.send("only usable in the following channels: {0}".format(
                    ", ".join([message.guild.get_channel(x).mention for x in self.support_command_use_channels])))

        if command == "anarchy-nutshell":
            await message.channel.send(self.texts["anarchism-in-a-nutshell"])

        if command == "echo":
            await message.channel.send(message.author.display_name + " wants me to say:\n" + " ".join(message_parts).replace("@", "@​")) #second @ has a zwsp

        if command == "guess":
            try:
                lim = int(message_parts[0])
                if lim <= 0:
                    return await message.channel.send(
                        "values under 1 are unsupported because julia is too lazy to code it")
            except IndexError:
                lim = 10
            except ValueError:
                return await message.channel.send("it needs to be a number, dork")

            await message.channel.send("Guess a number between 1 and {}.".format(lim))

            def is_correct(m):
                return m.author == message.author and m.content.isdigit()

            answer = random.randint(1, lim)

            while True:
                try:
                    guess = await self.wait_for('message', check=is_correct, timeout=5.0)
                except asyncio.TimeoutError:
                    return await message.channel.send(
                        "Sorry {0}, you took too long to respond (5 seconds)\nAnswer was {1}.".format(
                            message.author.display_name, answer))

                if int(guess.content) == answer:
                    await message.channel.send("You got it right!")
                    break
                elif int(guess.content) > answer:
                    await message.channel.send("Too high")
                else:
                    await message.channel.send("Too low")

        if command == "tmpmsg":
            timeout = int(message_parts[0])
            await asyncio.sleep(timeout)
            await message.delete()

        if command == "invite":
            await message.channel.send(self.invite_link + " beep boop :3")

        if command == "create":
            if "anarchy" in str(self.get_channel(message.channel.category_id)):
                anarchy_category = self.get_channel(message.channel.category_id)
                anarchy_log_channel = self.get_channel(self.anarchy_log_id)
                anarchy_role = message.guild.get_role(self.anarchy_role_id)
                valid_role = message.guild.get_role(self.valid_role_id)
                everyone_role = message.guild.default_role
                bot_role = message.guild.get_role(self.bot_role_id)

                try:
                    channel_name = message_parts[0]
                except IndexError:
                    channel_name = message.author.name + "-is-an-idiot-and-cannot-put-a-channel-name"

                overwrites = {anarchy_role: discord.PermissionOverwrite(manage_channels=True, read_messages=True,
                                                                        manage_roles=True),
                              everyone_role: discord.PermissionOverwrite(read_messages=False),
                              valid_role: discord.PermissionOverwrite(read_messages=False),
                              bot_role: discord.PermissionOverwrite(read_messages=True)}

                if len(anarchy_category.text_channels) >= 50:
                    await anarchy_log_channel.send(
                        "{0.mention} tried to create #{1}, but the limit was reached".format(message.author,
                                                                                             channel_name))
                    return await message.channel.send(
                        "channel limit reached (50). delete a channel before creating another")
                else:
                    new_channel = await anarchy_category.create_text_channel(channel_name, overwrites=overwrites)
                    await message.channel.send(
                        "channel {0.mention} created, anyone can edit the permissions".format(new_channel))
                    await anarchy_log_channel.send(
                        "{0.mention} created channel {1.mention} from {2.mention}".format(message.author, new_channel,
                                                                                          message.channel))
            else:
                await message.channel.send("only allowed in the anarchy section")

        if command == "googleit":
            if len(message_parts):
                await message.channel.send(
                    "https://lmgtfy.com/?q=" + urlencode(" ".join(message_parts)).replace("%20", "+"))
            else:
                messages = await message.channel.history(limit=2).flatten()
                await message.channel.send(
                    "https://lmgtfy.com/?q=" + urlencode(messages[1].content).replace("%20", "+"))

        if command == "delete":
            if "anarchy" in str(self.get_channel(message.channel.category_id)):
                # anarchy_category = self.get_channel(message.channel.category_id)  # just in case it's needed
                channel_id = message.channel.id
                # channel_protected = self.get_channel(self.protected_channel_id)
                anarchy_log_channel = self.get_channel(self.anarchy_log_id)
                # channel_info = self.get_channel(self.anarchy_info_channel_id)

                if channel_id not in self.protected_anarchy_channels:
                    await message.channel.send(
                        "deleting channel {}, anyone can type `!abort` within 10 seconds to cancel".format(
                            message.channel.mention))
                    await anarchy_log_channel.send(
                        "{0.mention} started a delete command in channel #{1.name}".format(message.author,
                                                                                           message.channel))

                    def is_correct(m):
                        if message.channel == m.channel and m.content == "!abort":
                            anarchy_log_channel.send(
                                "delete of #{0.name} aborted by {1.mention}".format(self.get_channel(channel_id),
                                                                                    m.author))
                            return True
                        else:
                            return False

                    try:
                        await self.wait_for('message', check=is_correct, timeout=10.2)
                        await message.channel.send("aborted")
                    except asyncio.TimeoutError:
                        await anarchy_log_channel.send("deleted #{0.name}".format(self.get_channel(channel_id)))
                        await self.get_channel(channel_id).delete()
                else:
                    await message.channel.send("this channel is protected and can not be deleted")
            else:
                await message.channel.send("only allowed in the anarchy section")

        if command == "userinfo":
            if "chat" in message_parts:
                return_channel = message.channel
            else:
                if message.author.dm_channel:
                    return_channel = message.author.dm_channel
                else:
                    return_channel = await message.author.create_dm()

            a = message.author
            response_text = "```\n"
            response_text += "User:       " + str(a) + "\n"
            response_text += "ID:         " + str(a.id) + "\n"
            response_text += "Created at: " + a.created_at.strftime("%d/%m/%Y, %H:%M:%S") + "\n"

            response_text += "\nServer specific:\n"
            response_text += "Joined at: " + a.joined_at.strftime("%d/%m/%Y, %H:%M:%S") + "\n"
            response_text += "Nickname:  " + a.display_name + "\n"

            response_text += "```"

            await return_channel.send(response_text)
            if "chat" not in message_parts:
                await message.channel.send("sent :3")

        if command == "meme-images":
            if "bot-spam" in message.channel.name or "anarchy" in str(self.get_channel(message.channel.category_id)):
                for image in self.images:
                    await self.send_meme_image(message.channel, image, text='"'+image+'"')
            return  # we don't want to interpret that as a meme command now do we

        if message.content.startswith(self.command_prefix + "meme"):
            rest_text = message.content[len(self.command_prefix)+5:]  # trim away prefix + "meme" + one character of whitespace
            lines = [x for x in rest_text.split("\n") if x.strip() != ""]  # get non empty lines

            for line in lines:
                if ":" in line:
                    parts = line.split(":")
                    image = parts[0].strip()
                    text = ":".join(parts[1:]).strip()
                else:
                    image, text = line, ""

                try:
                    await self.send_meme_image(message.channel, image, text=text)
                except KeyError:
                    return await message.channel.send(f'invalid image: "{image}"\ncheck the docs channel')

        if command == "spoiler":
            if len(message.attachments) < 1: #check that the user actually attached an image
                return await message.channel.send("No images attached!")
            images = []
            for a in message.attachments:
                async with aiohttp.ClientSession() as session:
                    async with session.get(a.url) as response:
                        data = io.BytesIO(await response.read()) #load it into memory
                        images.append(discord.File(data, 'SPOILER_'+a.filename)) #append to images
            await message.delete() #delete original message now that we've downloaded the files
            text = f"Spoilered image by **{message.author.display_name}**" 
            if message_parts: #if the user added a message after !spoiler, add that to our message
                text = text + ": " + " ".join(message_parts)
            await message.channel.send(text.replace("@", "@​"), files=images) #second @ has zwsp, send all images at once

nyandroid = NyandroidClient()
nyandroid.run(TOKEN)
